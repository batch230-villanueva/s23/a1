// No.1

const trainer = {
    name: "Nekonic",
    age: 22,
    pokemon: ["Pikachu", "Espeon", "Greninja", "Hoopa", "Venusaur", "Dragonite"],
    friends: {
        expert: ["Tulfone_user", "MazKazov"],
        great: ["1KA3OD", "Ehipto29"],
    },

    // METHODS
    talk: () => {
        console.log("Pikachu! I choose you");
    }
}

console.log(trainer);

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of bracket notation:");
console.log(trainer["pokemon"]);

console.log("Result of talk method:");
trainer.talk();


// No. 2

function Pokemon(name, level) {
    this.name = name;
    this.level = level;
    this.health = level * 2;
    this.attack = level;

    this.tackle = (target) => {
        console.log(`${this.name} tackled ${target.name}!`);
        target.health = target.health - this.attack;
        console.log(`${target.name}'s health is now reduced to ${target.health}`);

        if(target.health <= 0) {
            target.faint();
        }
    };

    this.faint = () => {
        console.log(`${this.name} has fainted.`);
    }


}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

// No. 3

geodude.tackle(pikachu);

console.log(pikachu);

mewtwo.tackle(geodude);

console.log(geodude)






